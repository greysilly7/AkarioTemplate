import { Listener } from 'discord-akairo';

export default class ReadyListener extends Listener {
	public constructor() {
		super('ready', {
			emitter: 'client',
			event: 'ready',
		});
	}

	public async exec(): Promise<void> {
		console.info(`Logged in as ${this.client.user?.tag}`);
		console.info(`Currently in ${this.client.guilds.cache.size} guilds`);
		this.client.user?.setPresence({
			activity: {
				name: `${this.client.config.prefix}help`,
				type: 'PLAYING',
			},
			status: 'online',
		});

		if (this.client.voice.connections.size > 0) {
			this.client.voice?.connections.forEach(connection => {
				connection.channel.leave();
			});
		}
	}
}
