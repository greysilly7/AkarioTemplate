import { Command } from 'discord-akairo';
import { Message } from 'discord.js';
export default class PingCommand extends Command {
	public constructor() {
		super('ping', {
			aliases: ['ping'],
			description: {
				// eslint-disable-next-line prettier/prettier
				content: 'Gets the bot\'s heartbeat and latency',
				usage: '',
				examples: []
			},
			ratelimit: 2,
			category: 'utility'
		});
	}

	public async exec(message: Message): Promise<Message> {
		const embed = this.client.util
			.embed()
			.setColor([155, 200, 200])
			.setDescription('Pinging...');
		const ping = await message.util.send(embed);
		const embed1 = this.client.util
			.embed()
			.setColor('RANDOM')
			.setDescription(`🏓 **${Math.round(this.client.ws.ping)}**ms`)
			.setFooter(
				`Latentcy is ${
					ping.createdTimestamp - message.createdTimestamp
				}ms`
			);
		return ping.edit(embed1);
	}
}
