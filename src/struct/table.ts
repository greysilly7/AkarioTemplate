import { table, ValueData } from 'quick.db';
import { writeFile } from 'fs/promises';
export default class Table extends table {
	constructor(tableName: string) {
		super(tableName);
		this.tableName = tableName;
	}

	public ensure(key: string, value: ValueData): void {
		if (!this.has(key)) {
			return this.set(key, value);
		}
	}

	public async math(
		key: string,
		operator: '+' | '-' | '*' | '%' | '/',
		value: string | number
	): Promise<number> {
		const value1 = parseInt(this.get(key));
		const value2 = parseInt(value.toString());
		switch (operator) {
			case '+':
				this.set(key, value1 + value2);
				return value1 + value2;
			case '-':
				this.set(key, value1 - value2);
				return value1 - value2;
			case '*':
				this.set(key, value1 * value2);
				return value1 * value2;
			case '%':
				this.set(key, value1 % value2);
				return value1 % value2;
			case '/':
				this.set(key, value1 / value2);
				return value1 / value2;
			default:
				throw new Error('Unknown operator');
		}
	}

	public currentTableName(): string {
		return this.tableName;
	}

	public backupTable(): Promise<void> {
		const table = this.all();
		const jsonTable = JSON.stringify(Object.assign({}, table));
		return writeFile('./dump.json', jsonTable);
	}
}
